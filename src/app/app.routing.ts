import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout/auth-layout.component';
import { PageNotFoundComponent } from './common/error/page-not-found/page-not-found.component'


export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'admin/items/list',
        pathMatch: 'full',
    }, {
        path: 'admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './admin/items/items.module#ItemsModule'
            },
            {
                path: '',
                loadChildren: './admin/typicode/typicode.module#TypicodeModule'
            }
        ]
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: '',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    },
    { path: '**', component: PageNotFoundComponent }
];
