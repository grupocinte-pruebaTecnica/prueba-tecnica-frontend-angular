import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';

import { RouterModule } from '@angular/router';
import { PagesRoutes } from './pages.routing';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes)
  ]
})
export class PagesModule { }
