import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout/auth-layout.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout/admin-layout.component';

import { HttpClientModule } from '@angular/common/http';


import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';
import { PageNotFoundComponent } from './common/error/page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    AdminLayoutComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
