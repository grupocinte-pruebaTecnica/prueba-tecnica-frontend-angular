import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemsService } from './../../../../services/items.service';
import { Router } from '@angular/router';

import { ItemModel } from 'src/app/models/Item.model';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnInit {

  itemForm: ItemModel;
  id: String

  title: String = "Crear Item"
  titleBtnSubmit: String = "Guardar"
  
  formEdit = false;






  constructor(private _activatedRoute: ActivatedRoute, private _router: Router, private _itemsService: ItemsService) {
    this.itemForm = new ItemModel();
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
    if (this.id != null) {
      this.title = "Editar item";
      this.getDetailItem()
      this.formEdit = true;
      this.titleBtnSubmit = "Actualizar"
    }
    
  }

  ngOnInit() {
  }

 

  onSubmitForm() {
    //validar formulario

    if(this.formEdit){
      this._itemsService.updateItem(this.itemForm)
      .then(res => {
        alert("Item Actualizado correctamente")
        this._router.navigate(['/admin/items/list']);
        console.log(res);
      }).catch((err) => {
        console.log(err);
      });
    } else{
      this._itemsService.createItem(this.itemForm)
      .then(res => {
        alert("Item creado correctamente")
        this._router.navigate(['/admin/items/list']);
        console.log(res);
      }).catch((err) => {
        console.log(err);
      });
    }



    
  }

  private getDetailItem() {
    this._itemsService.detailItem(this.id)
    .then(res => {
      this.itemForm = res
    }).catch((err) => {
      console.log(err);
    });
  }



}
