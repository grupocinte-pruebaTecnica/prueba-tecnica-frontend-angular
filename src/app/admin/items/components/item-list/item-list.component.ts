import { Component, OnInit } from '@angular/core';
import { ItemsService } from './../../../../services/items.service';
import { ItemModel } from 'src/app/models/Item.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  constructor(private _itemsService: ItemsService) { }

  listItems: ItemModel[] = [];

  ngOnInit() {
    this.getAllItems();
  }

  getAllItems(){
    this._itemsService.getAllItems()
    .then(res => {
      this.listItems = res
      console.log(res);
    }).catch((err) => {
      console.log(err);
    });
  }

  editItem(id) {
    alert("editar el item con id: " + id)
  }
  deleteItem(id) {
    if (confirm("Vas a eliminar el item con id: " + id)) {
     
      this._itemsService.deleteItem(id)
        .then(res => {
          alert("Item eliminado correctamente")
          this.getAllItems();
        }).catch((err) => {
          console.log(err);
        });
    }


  }

}
