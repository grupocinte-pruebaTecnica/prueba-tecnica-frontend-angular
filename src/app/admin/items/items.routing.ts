import { Routes } from '@angular/router';

import { ItemsComponent } from './components/items/items.component';
import { ItemFormComponent } from './components/item-form/item-form.component';

export const ItemsRoutes: Routes = [
    {

      path: 'items',
      children: [ {
        path: 'list',
        component: ItemsComponent
      },
      {
        path: 'create',
        component: ItemFormComponent
    }
    ,
      {
        path: 'update/:id',
        component: ItemFormComponent
    }
  
  
  ]
}
];