import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsComponent } from './components/items/items.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { ItemsRoutes } from './items.routing';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { ItemListComponent } from './components/item-list/item-list.component';

@NgModule({
  declarations: [ItemsComponent, ItemFormComponent, ItemListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ItemsRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ItemsModule { }
