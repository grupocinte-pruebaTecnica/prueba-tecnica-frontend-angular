import { Routes } from '@angular/router';

import { TypicodeComponent } from './components/typicode/typicode.component';


export const TypicodeRoutes: Routes = [
    {

      path: 'typicode',
      children: [ {
        path: 'list',
        component: TypicodeComponent
      }
  
  
  ]
}
];