import { Component, OnInit } from '@angular/core';
import { TypicodeService } from './../../../../services/typicode.service';
import { ItemsModule } from 'src/app/admin/items/items.module';
@Component({
  selector: 'app-typicode',
  templateUrl: './typicode.component.html',
  styleUrls: ['./typicode.component.scss']
})
export class TypicodeComponent implements OnInit {

  list: ItemsModule[] = []
  constructor(private _typicodeService: TypicodeService) { }

  ngOnInit() {
    this.getAll()
  }

  getAll() {
    this._typicodeService.getAllItems()
      .then(res => {
        this.list = res
        console.log(res);
      }).catch((err) => {
        console.log(err);
      });
  }

}
