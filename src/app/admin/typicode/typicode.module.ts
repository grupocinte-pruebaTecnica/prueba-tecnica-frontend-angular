import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypicodeComponent } from './components/typicode/typicode.component';

import { RouterModule } from '@angular/router';
import { TypicodeRoutes } from './typicode.routing';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [TypicodeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(TypicodeRoutes),
    HttpClientModule
  ]
})
export class TypicodeModule { }
