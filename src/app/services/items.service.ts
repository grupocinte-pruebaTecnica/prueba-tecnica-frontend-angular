import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemModel } from '../models/Item.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private _http: HttpClient) { }

  getAllItems() {
    return this._http.get<ItemModel[]>(environment.urlApi+"/api/item")
      .toPromise()
      
  }
  detailItem(id: String) {
    return this._http.get<ItemModel>(environment.urlApi+"/api/item/"+id)
      .toPromise()
      
  }
  
  createItem(item:ItemModel) {
    return this._http.post<ItemModel>(environment.urlApi+"/api/item", item)
      .toPromise() 
  }

  updateItem(item:ItemModel) {
    return this._http.put<ItemModel>(environment.urlApi+"/api/item/"+item.id, item)
      .toPromise() 
  }

  deleteItem(id: String) {
    return this._http.delete<any>(environment.urlApi+"/api/item/"+id)
      .toPromise()
      
  }

}
