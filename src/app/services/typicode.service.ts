import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemModel } from '../models/Item.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypicodeService {

  constructor(private _http: HttpClient) { }

  getAllItems() {
    return this._http.get<ItemModel[]>(environment.urlTypicode+"/todos")
      .toPromise()
      
  }
  
}
